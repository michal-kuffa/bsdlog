from setuptools import setup


setup(
    name='bsdlog',
    version='0.0.1',
    packages=['bsdlog'],
    extras_require={
        'test': [
            'pytest',
            'pytest-cov',
            'mypy',
            'pylint',
        ],
    },
    entry_points={
        'console_scripts': [
            'bsdlog = bsdlog.__main__:main',
        ]
    }
)
