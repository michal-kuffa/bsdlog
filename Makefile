.PHONY: install-test
install-test:
	pip install --editable .[test]

.PHONY: lint
lint:
	pylint bsdlog || echo "Your code is whack :("

.PHONY: mypy
mypy:
		mypy bsdlog

.PHONY: pytest
pytest:
	python -m pytest tests --cov bsdlog --cov-report term-missing

.PHONY: test
test: install-test lint mypy pytest


.PHONY: test-gen
test-gen:
	python -m bsdlog.generate --num 100 | python -m bsdlog --proc 4 --chunk 56 2>/dev/null
	@python -m bsdlog.generate --num 100 | python -m bsdlog --proc 4 --chunk 56 2>/dev/null | grep "Total messages: 100"
