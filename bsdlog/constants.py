"""
bsdlog.constants
~~~~~~~~~~~~~~~~

RFC 3164 related constants:

    * Facility
    * Severity
"""

from typing import Dict

from enum import Enum


FACILITIES: Dict = {
    0: "kernel messages",
    1: "user-level messages",
    2: "mail system",
    3: "system daemons",
    4: "security/authorization messages (note 1)",
    5: "messages generated internally by syslogd",
    6: "line printer subsystem",
    7: "network news subsystem",
    8: "UUCP subsystem",
    9: "clock daemon (note 2)",
    10: "security/authorization messages (note 1)",
    11: "FTP daemon",
    12: "NTP subsystem",
    13: "log audit (note 1)",
    14: "log alert (note 1)",
    15: "clock daemon (note 2)",
    16: "local use 0  (local0)",
    17: "local use 1  (local1)",
    18: "local use 2  (local2)",
    19: "local use 3  (local3)",
    20: "local use 4  (local4)",
    21: "local use 5  (local5)",
    22: "local use 6  (local6)",
    23: "local use 7  (local7)",
}


class SeverityNum(Enum):
    Emergency = 0
    Alert = 1
    Critical = 2
    Error = 3
    Warning = 4
    Notice = 5
    Informational = 6
    Debug = 7


SEVERITIES: Dict = {
    SeverityNum.Emergency.value: "Emergency: system is unusable",
    SeverityNum.Alert.value: "Alert: action must be taken immediately",
    SeverityNum.Critical.value: "Critical: critical conditions",
    SeverityNum.Error.value: "Error: error conditions",
    SeverityNum.Warning.value: "Warning: warning conditions",
    SeverityNum.Notice.value: "Notice: normal but significant condition",
    SeverityNum.Informational.value: "Informational: informational messages",
    SeverityNum.Debug.value: "Debug: debug-level messages",
}


MONTHS = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12,
}

MONTHS_REV = {v: k for k, v in MONTHS.items()}
