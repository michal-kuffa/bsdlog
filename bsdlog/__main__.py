"""
bsdlog
~~~~~~

Read BSD Syslog messages and print out statistics.

From give `LOG_FILE` reads and parse BSD Syslog messages which must adhere to
RFC 3164. After the file is parsed, printout total and pre-host statistics.

Usage examples
--------------

  Simple usage for local file
  ---------------------------

    $ python -m bsdlog app.log

  Force 4 proccesses, default is number of cores
  ----------------------------------------------

    $ python -m bsdlog app.log --proc 4

  Stream from remote location
  ---------------------------

  For slow/fast streams you can play with `--chunk` size and number of `--proc`
  to get the best performace.

    $ wget -qO - https://example.com/logs/app.log.tar.gz \\
        | tar -xzf - -O \\
        | python -m bsdlog --chunk 4096

  Stream from the testing, generate module
  ----------------------------------------

    $ python -m bsdlog.generate --num 1000 | python -m bsdlog

"""

import os
import sys
import typing
import logging
import argparse

from multiprocessing import Process, Lock, Queue, cpu_count

from bsdlog.reader import reader_process
from bsdlog.stats import ParsingStats


def get_argv_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        'log_file',
        metavar='LOG_FILE',
        nargs='?',
        help="Log file with BSD Syslog messages. Defaults to standard input."
    )
    parser.add_argument(
        '--proc',
        type=int,
        metavar='NUM_PROCESSES',
        default=cpu_count(),
        help="Number of child processes. Default is number of CPUs."
    )
    parser.add_argument(
        '--chunk',
        type=int,
        metavar='CHUNK_SIZE',
        default=10 * (2 ** 20),
        help="Chunk for parsing by each process in bytes. Default is 10MB."
    )
    parser.add_argument(
        '--encoding',
        type=str,
        metavar='ENCODING',
        default=sys.getdefaultencoding(),
        help="Log file encoding. Defaults to system encoding."
    )
    return parser


def main(argv: typing.Optional[typing.Sequence[str]] = None) -> None:
    args = get_argv_parser().parse_args(sys.argv[1:] if argv is None else argv)

    logging.basicConfig(
        level=logging.INFO,
        format=(
            "[%(asctime)s][pid:%(process)d]"
            "[%(module)s:%(lineno)d]: %(message)s"
        )
    )

    if args.log_file is None:
        original_fd = sys.stdin.fileno()
    else:
        try:
            original_fd = os.open(args.log_file, os.O_RDONLY)
        except FileNotFoundError:
            sys.exit(f"File `{args.log_file}` does not exist.")
        except PermissionError:
            sys.exit(f"Insufficient permissions to read `{args.log_file}`.")

    input_fd: int = 1000
    os.dup2(original_fd, input_fd)
    lock: typing.ContextManager = Lock()

    stats = ParsingStats()

    if args.proc == 1:
        logging.info("Single reader process run")
        stats.combine(
            reader_process(input_fd, lock, None, args.chunk, args.encoding))
    else:
        stats_queue: Queue = Queue()
        readers = [
            Process(
                target=reader_process,
                args=(input_fd, lock, stats_queue, args.chunk, args.encoding))
            for _ in range(args.proc)
        ]

        for reader in readers:
            reader.start()

        for reader in readers:
            reader.join()

        logging.info("All reader processes finished!")

        for reader in readers:
            if reader.exitcode:
                sys.exit(f'Child process `{reader.pid}` crashed!')
            stats.combine(stats_queue.get())

    for host_stats in stats.values():
        print(host_stats.pprint_format())
    print(stats.pprint_format())

    if not os.isatty(input_fd):
        os.close(input_fd)
        os.close(original_fd)


if __name__ == '__main__':
    main()  # pragma: no cover
