"""
bsdlog.stats
~~~~~~~~~~~~

Helpers for creating and keeping statistics of parsed messaages.
"""
from textwrap import dedent
from typing import TypeVar, Optional, Dict, Callable
from collections import UserDict

from bsdlog.constants import SeverityNum
from bsdlog.parsing import (
    Message, Severity, Timestamp
)


TSeverityStats = TypeVar('TSeverityStats', bound='SeverityStats')


class SeverityStats:

    def __init__(self, severity_num: int) -> None:
        self.total_count: int = 0
        self.avg_msg_len: float = 0
        self.severity: Severity = Severity.from_num(severity_num)
        self.oldest: Optional[Timestamp] = None
        self.newest: Optional[Timestamp] = None

    @property
    def total_msg_len(self) -> float:
        return self.total_count * self.avg_msg_len

    def combine(self, stats: TSeverityStats) -> None:
        self.combine_newest(stats.newest)
        self.combine_oldest(stats.oldest)
        self.combine_avg_msg_len(stats.total_msg_len, stats.total_count)
        self.total_count += stats.total_count

    def combine_avg_msg_len(
        self, total_msg_len: float, total_count: int
    ) -> None:
        self.avg_msg_len = (
            (self.total_msg_len + total_msg_len)
            /
            (self.total_count + total_count)
        )

    def combine_oldest(self, oldest: Optional[Timestamp]) -> None:
        if oldest and (self.oldest is None or oldest < self.oldest):
            self.oldest = oldest

    def combine_newest(self, newest: Optional[Timestamp]) -> None:
        if newest and (self.newest is None or newest > self.newest):
            self.newest = newest

    def add_msg(self, msg: Message) -> None:
        msg_dt: Timestamp = msg.timestamp_dt
        self.combine_newest(msg_dt)
        self.combine_oldest(msg_dt)
        self.combine_avg_msg_len(len(msg.MSG), 1)
        self.total_count += 1


TMultiStats = TypeVar('TMultiStats', bound='MultiStats')


class MultiStats(UserDict):  # pylint:disable=too-many-ancestors

    @property
    def oldest(self) -> Optional[Timestamp]:
        return min(
            filter(None, (s.oldest for s in self.values())),
            default=None,
        )

    @property
    def newest(self) -> Optional[Timestamp]:
        return max(
            filter(None, (s.newest for s in self.values())),
            default=None,
        )

    @property
    def total_count(self) -> int:
        return self.filtered_total_count()

    def filtered_total_count(self, filterfn: Optional[Callable] = None) -> int:
        return sum(s.total_count for s in filter(filterfn, self.values()))

    @property
    def total_msg_len(self) -> int:
        return sum(s.total_msg_len for s in self.values())

    @property
    def avg_msg_len(self) -> float:
        # might be heavy to calculate
        total_count = self.total_count
        return total_count and self.total_msg_len / total_count

    def combine(self, stats: TMultiStats) -> None:
        for key, stat in stats.items():
            self[key].combine(stat)


THostStats = TypeVar('THostStats', bound='HostStats')


class HostStats(MultiStats):  # pylint:disable=too-many-ancestors

    def __init__(self, host: str, initialdata: Optional[Dict] = None) -> None:
        self.host = host
        super().__init__(initialdata)

    def add_msg(self, msg: Message) -> None:
        self[msg.severity_num].add_msg(msg)

    def pprint_format(self) -> str:
        return dedent("""
            Hostname: {self.host}
                Average length of the MSG part: {self.avg_msg_len}
                Total messages: {self.total_count}
                Emergency total: {emergency_total_count}
                Alert total: {alert_total_count}
                Oldest timestamp: {oldest}
                Newest timestamp: {newest}
        """.format(
            self=self,
            emergency_total_count=self.filtered_total_count(
                lambda s: s.severity.num == SeverityNum.Emergency.value
            ),
            alert_total_count=self.filtered_total_count(
                lambda s: s.severity.num == SeverityNum.Alert.value
            ),
            oldest=self.oldest and self.oldest.as_str,
            newest=self.newest and self.newest.as_str,
        ))

    def __getitem__(self, severity_num: int) -> SeverityStats:
        if severity_num not in self:
            self[severity_num] = SeverityStats(severity_num)
        return super().__getitem__(severity_num)


TParsingStats = TypeVar('TParsingStats', bound='ParsingStats')


class ParsingStats(MultiStats):  # pylint:disable=too-many-ancestors

    def add_msg(self, msg: Message) -> None:
        self[msg.HOSTNAME].add_msg(msg)

    def pprint_format(self) -> str:
        return dedent("""
            Total:
                Average length of the MSG part: {self.avg_msg_len}
                Total messages: {self.total_count}
                Emergency total: {emergency_total_count}
                Alert total: {alert_total_count}
                Oldest timestamp: {oldest}
                Newest timestamp: {newest}
        """.format(
            self=self,
            emergency_total_count=sum(
                hs.filtered_total_count(
                    lambda s: s.severity.num == SeverityNum.Emergency.value
                )
                for hs in self.values()
            ),
            alert_total_count=sum(
                hs.filtered_total_count(
                    lambda s: s.severity.num == SeverityNum.Alert.value
                )
                for hs in self.values()
            ),
            oldest=self.oldest and self.oldest.as_str,
            newest=self.newest and self.newest.as_str,
        ))

    def __getitem__(self, host: str) -> HostStats:
        if host not in self:
            self[host] = HostStats(host)
        return super().__getitem__(host)
