"""
bsdlog.generate
~~~~~~~~~~~~~~~

Module used to generate BSD Syslog messages compliant with
`RFC 3164: The BSD syslog Protocol<https://tools.ietf.org/html/rfc3164>`_.

Used solely for testing.
"""
import sys
import argparse
from random import choice
from typing import Sequence, Optional, List
from bsdlog.parsing import Timestamp
from bsdlog.constants import FACILITIES, SEVERITIES, MONTHS


def get_argv_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--num',
        type=int,
        metavar='NUM_MSGS',
        default=1000000,
        help="Number of messages. Default 1000000"
    )
    return parser


def main(argv: Optional[Sequence[str]] = None) -> None:
    """
    Main entrypoint for BSD Syslog messages generator.
    """
    args = get_argv_parser().parse_args(sys.argv[1:] if argv is None else argv)

    facitilies: List[int] = list(FACILITIES.keys())
    severities: List[int] = list(SEVERITIES.keys())
    months: List[int] = list(MONTHS.values())
    days: List[int] = list(range(1, 29))
    hours: List[int] = list(range(0, 24))
    minutes: List[int] = list(range(0, 60))
    seconds: List[int] = list(range(0, 60))
    hostnames: List[str] = [
        'localhost', '127.0.0.1', 'mymachine', '8.8.8.8',
    ]
    msg_lengths: List[int] = list(range(50, 101))

    for _ in range(args.num):
        print("<{priority_num}>{timestamp.as_str} {hostname} {msg}".format(
            priority_num=choice(facitilies) * 8 + choice(severities),
            timestamp=Timestamp(
                choice(months),
                choice(days),
                choice(hours),
                choice(minutes),
                choice(seconds),
            ),
            hostname=choice(hostnames),
            msg="A" * choice(msg_lengths),
        ))


if __name__ == '__main__':
    main()  # pragma: no cover
