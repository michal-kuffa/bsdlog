"""
bsdlog.parsing
~~~~~~~~~~~~~~

RFC 3164 messages parsing utilities.
"""

import re
from typing import Tuple, Dict, Pattern
from collections import namedtuple
from bsdlog.constants import (
    FACILITIES, SEVERITIES,
    MONTHS, MONTHS_REV
)


MSG_RE: Pattern = re.compile(
    r"(?P<PRI>[<]\d{1,3}[>])"
    r"(?P<TIMESTAMP>[A-Z][a-z]{2}[ ]{1,2}\d{1,2}[ ]\d{2}:\d{2}:\d{2})"
    r" "
    r"(?P<HOSTNAME>[^ ]+)"
    r" "
    r"(?P<MSG>.*)"
)


def parse_priority(pri: str) -> Tuple[int, int]:
    """
    Parses PRI part of message returning tuple with facility and severity.
    """
    priority = int(pri[1:-1])
    return priority // 8, priority % 8


class PRIPart(namedtuple("PRIPartBase", ["num", "name"])):
    MAP: Dict = {}

    @classmethod
    def from_num(cls, num: int):
        return cls(num, cls.MAP[num])


class Facility(PRIPart):
    MAP = FACILITIES


class Severity(PRIPart):
    MAP = SEVERITIES


class Timestamp(
    namedtuple(
        "Timestamp",
        ["month", "day", "hour", "minute", "second"]
    )
):

    @classmethod
    def from_str(cls, timestamp: str):
        month, day, timestr = timestamp.split()
        hour, minute, second = timestr.split(':')
        return cls(
            MONTHS[month], int(day),
            int(hour), int(minute), int(second)
        )

    @property
    def as_str(self):
        return (
            f"{MONTHS_REV[self.month]} {self.day:>2}"
            f" {self.hour:0>2}:{self.minute:0>2}:{self.second:0>2}"
        )


class Message(
    namedtuple(
        "MessageBase",
        ["PRI", "TIMESTAMP", "HOSTNAME", "MSG"],
    )
):

    def parse_priority(self) -> Tuple[int, int]:
        return parse_priority(self.PRI)

    @property
    def facility_num(self) -> int:
        return self.parse_priority()[0]

    @property
    def severity_num(self) -> int:
        return self.parse_priority()[1]

    @property
    def timestamp_dt(self) -> Timestamp:
        return Timestamp.from_str(self.TIMESTAMP)

    @classmethod
    def from_str(cls, msg: str):
        return cls(*MSG_RE.match(msg).groups())  # type: ignore
