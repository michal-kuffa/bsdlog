"""
bsdlog.reader
~~~~~~~~~~~~~

Functionality for reading, parsing file in child process and passing statistics
back to the parent.
"""

import os
import logging
from io import BytesIO
from typing import ContextManager, Optional
from multiprocessing import Queue

from bsdlog.stats import ParsingStats
from bsdlog.parsing import Message


def read_till_newline_or_eof(input_fd: int, buf: BytesIO) -> None:
    while True:
        byte = os.read(input_fd, 1)
        if not byte:
            return
        buf.write(byte)
        if byte == b'\n':
            return


def reader_process(
    input_fd: int,
    lock: ContextManager,
    stats_queue: Optional[Queue],
    chunk_size: int = 4096,
    encoding: str = 'utf-8'
) -> ParsingStats:
    logging.info(
        "New reader initialized: chunk:%s, enc:%s",
        chunk_size, encoding
    )
    stats: ParsingStats = ParsingStats()
    while True:
        buf: BytesIO = BytesIO()
        with lock:
            bytes_read = buf.write(os.read(input_fd, chunk_size))
            if not bytes_read:
                break
            read_till_newline_or_eof(input_fd, buf)
        buf.seek(0)
        for bytes_line in buf:
            stats.add_msg(Message.from_str(bytes_line.decode(encoding)))
        buf.close()
        logging.info("Processed total %s of messages", stats.total_count)
    if stats_queue:
        stats_queue.put(stats)
    logging.info("Finished successfully")
    return stats
