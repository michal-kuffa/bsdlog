
======
bsdlog
======

Application to process BSD Syslog messages and output some statistics.


Approach to the problem
-----------------------

Given filename open the file for reading and save file-descriptor. If stdin is
used as an input, use its file-descriptor. Duplicates the file-descriptor
(os.dup2) so it's inheritable.

Spawn child processes with the inheritable file-descriptor as an argument.
Additional arguments being lock for synchronization of the reading and queue
for sending back results of parsing back to the parent process.

Repeatedly, child process acquires lock and reads chunk (default 10MB) of bytes
from the file descriptor. Afterwards reads byte-by-byte until it hits either
newline or EOF only then it releases the lock. All reads go to in-memory buffer
for further processing.

Processing of the buffer goes line by line while creating statistics about
processed messages. When the whole buffer is processed, child process tries
again to acquire lock and repeat process described above.

When child process encounter empty read (EOF) it sends statistics to the queue
and ends.

Parent waits for all child processes to finish parsing before it collects and
combines all statistics from the queue and outputs the combined statistics.


Requirements
------------

Developed and test only with Python 3.6. For running there are no dependencies
out of standard library.


Usage
-----

Check the module usage.

.. code-block:: bash

   $ python3.6 -m bsdlog --help


Testing & Dev
-------------

Create and activate separate Python 3.6 virtualenv.

.. code-block:: bash

   $ make test
   $ make test-gen


There's module to generate BSD Syslog messages


.. code-block:: bash

   $ python3.6 -m bsdlog.generate --help


Happy Hacking!
