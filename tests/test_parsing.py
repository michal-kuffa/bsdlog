from bsdlog.parsing import (
    parse_priority, Message, Facility, Severity,
    Timestamp
)


def test_timestamp():
    tsaug = "Aug  3 22:14:05"
    tsdec = "Dec  3 22:14:15"
    assert Timestamp.from_str(tsaug) < Timestamp.from_str(tsdec)
    assert Timestamp.from_str(tsaug).as_str == tsaug


def test_parse_priority():
    assert (20, 5) == parse_priority("<165>")
    assert (0, 0) == parse_priority("<0>")


def test_msg_from_str():
    tpl = "<165>Aug  3 22:14:05 localhost awesomeapp starting up version 1..."
    msg = Message.from_str(tpl)
    assert (
        msg.PRI == "<165>"
        and msg.TIMESTAMP == "Aug  3 22:14:05"
        and msg.HOSTNAME == "localhost"
        and msg.MSG == "awesomeapp starting up version 1..."
        and msg.facility_num == 20
        and msg.severity_num == 5
    )


def test_facility_from_num():
    assert Facility.from_num(3).name == 'system daemons'


def test_severity_from_num():
    assert Severity.from_num(2).name.startswith('Critical')
