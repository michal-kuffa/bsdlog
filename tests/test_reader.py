# flake8: noqa
import io
import os
from multiprocessing import Lock, Queue

from bsdlog.reader import read_till_newline_or_eof, reader_process


def test_read_till_newline_or_eof(tmpdir):
    # Empty file
    infile = tmpdir.mkdir('dir').join('input.log')
    with open(infile, 'wb') as inf:
        inf.write(b"")
    infile_fd = os.open(infile, os.O_RDONLY)
    buf = io.BytesIO()

    read_till_newline_or_eof(infile_fd, buf)
    assert not buf.getvalue()

    # Has newline
    with open(infile, 'wb') as inf:
        inf.write(b"bytes\nmorebytes")
    infile_fd = os.open(infile, os.O_RDONLY)
    buf = io.BytesIO()

    read_till_newline_or_eof(infile_fd, buf)
    assert buf.getvalue() == b"bytes\n"


# pylint:disable=line-too-long
def test_reader_process(tmpdir):
    infile = tmpdir.mkdir('dir').join('input.log')
    with open(infile, 'wb') as inf:
        inf.write(
            b"<47>Sep 22 15:38:21 mymachine myproc% fatal error, terminating!\n"
            b"<34>Jan 25 05:06:34 10.1.2.3 su: 'su root' failed for sprinkles on /dev/pts/8"
        )
    infile_fd = os.open(infile, os.O_RDONLY)

    queue = Queue()
    stats = reader_process(infile_fd, Lock(), queue)
    assert stats.total_count == 2

    stats = queue.get()
    assert stats.newest.day == 22
