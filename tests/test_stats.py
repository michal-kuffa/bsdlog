
from bsdlog.parsing import Message
from bsdlog.stats import SeverityStats, HostStats, ParsingStats


class TestSeverityStats:

    @classmethod
    def setup_class(cls):
        cls.msg_oldest = Message.from_str(
            "<165>Aug  3 22:14:15 localhost 1234567890")
        cls.msg_newest = Message.from_str(
            "<165>Dec  3 22:14:15 localhost 1234")

    def test_combine(self):
        severity_stats = SeverityStats(5)
        severity_stats.add_msg(self.msg_oldest)
        severity_stats.add_msg(self.msg_newest)

        severity_stats.combine(SeverityStats(5))

        assert severity_stats.total_count == 2
        assert severity_stats.avg_msg_len == 7

        severity_stats.combine(severity_stats)

        assert severity_stats.total_count == 4
        assert severity_stats.avg_msg_len == 7

        severity_stats2 = SeverityStats(5)
        for _ in range(severity_stats.total_count):
            severity_stats2.add_msg(
                Message.from_str("<165>Dec  20 22:14:15 localhost 123456789"))

        severity_stats.combine(severity_stats2)
        assert severity_stats.total_count == 8
        assert severity_stats.avg_msg_len == 8
        assert severity_stats.newest.day == 20
        assert severity_stats.oldest.month == 8

        severity_stats2 = SeverityStats(5)
        for _ in range(severity_stats.total_count):
            severity_stats2.add_msg(
                Message.from_str("<165>Jan  7 22:14:15 localhost 123456"))

        severity_stats.combine(severity_stats2)
        assert severity_stats.total_count == 16
        assert severity_stats.avg_msg_len == 7
        assert severity_stats.newest.day == 20
        assert severity_stats.oldest.month == 1

    def test_add_msg(self):
        severity_stats = SeverityStats(5)
        severity_stats.add_msg(self.msg_oldest)

        assert severity_stats.oldest == self.msg_oldest.timestamp_dt
        assert severity_stats.newest == self.msg_oldest.timestamp_dt
        assert severity_stats.avg_msg_len == len(self.msg_oldest.MSG)
        assert severity_stats.total_count == 1

        severity_stats.add_msg(self.msg_newest)
        assert severity_stats.oldest == self.msg_oldest.timestamp_dt
        assert severity_stats.newest == self.msg_newest.timestamp_dt
        assert severity_stats.avg_msg_len == 7
        assert severity_stats.total_count == 2


class TestHostStats:

    def test_add_msg(self):
        msg = Message.from_str("<165>Dec  3 22:14:15 localhost 1234")
        host_stats = HostStats("localhost")

        assert host_stats.total_count == 0
        assert host_stats.avg_msg_len == 0
        assert host_stats.oldest is None
        assert host_stats.newest is None

        host_stats.add_msg(msg)
        assert host_stats.total_count == 1
        assert host_stats.avg_msg_len == 4
        assert host_stats.oldest == msg.timestamp_dt
        assert host_stats.newest == msg.timestamp_dt

        msg_newest = msg
        msg = Message.from_str("<160>Aug  3 22:14:15 localhost 12345678")
        host_stats.add_msg(msg)
        assert host_stats.total_count == 2
        assert host_stats.avg_msg_len == 6
        assert host_stats.oldest == msg.timestamp_dt
        assert host_stats.newest == msg_newest.timestamp_dt
        assert host_stats[0].avg_msg_len == 8
        assert host_stats[5].avg_msg_len == 4

    def test_pprint_format(self):
        host_stats = HostStats("local")
        host_stats.add_msg(Message.from_str("<160>Dec  3 22:14:15 local 1234"))
        host_stats.add_msg(Message.from_str("<161>Dec  3 22:14:15 local 1234"))
        host_stats.add_msg(Message.from_str("<162>Dec  3 22:14:15 local 1234"))
        host_stats.add_msg(Message.from_str("<163>Dec  3 22:14:15 local 1234"))
        host_stats.add_msg(Message.from_str("<164>Dec  3 22:14:15 local 1234"))
        host_stats.add_msg(Message.from_str("<165>Dec  3 22:14:15 local 1234"))
        host_stats.add_msg(Message.from_str("<166>Dec  3 22:14:15 local 1234"))
        host_stats.add_msg(Message.from_str("<167>Dec  3 22:14:15 local 1234"))

        assert "Emergency total: 1" in host_stats.pprint_format()
        assert "Alert total: 1"in host_stats.pprint_format()

    def test_empty_severity_stats(self):
        host_stats = HostStats("localhost")
        host_stats[0] = SeverityStats(0)
        host_stats[1] = SeverityStats(1)

        assert host_stats.total_count == 0
        assert host_stats.oldest is None
        assert host_stats.newest is None

    def test_combine(self):
        msg = Message.from_str("<165>Dec  3 22:14:15 localhost 1234")
        host_stats = HostStats("localhost")
        host_stats.add_msg(msg)

        assert host_stats.total_count == 1
        assert host_stats.avg_msg_len == 4
        assert host_stats.oldest == msg.timestamp_dt
        assert host_stats.newest == msg.timestamp_dt

        host_stats.combine(host_stats)

        assert host_stats.total_count == 2
        assert host_stats.avg_msg_len == 4
        assert host_stats.oldest == msg.timestamp_dt
        assert host_stats.newest == msg.timestamp_dt
        assert host_stats[2].total_count == 0


class TestParsingStats:

    def test_parsing_stats(self):
        msg = Message.from_str("<165>Aug  3 22:14:15 localhost 1234")
        parsing_stats = ParsingStats()
        parsing_stats.add_msg(msg)
        parsing_stats.add_msg(msg)
        parsing_stats.add_msg(msg)

        assert parsing_stats.total_count == 3
        assert parsing_stats.avg_msg_len == 4
        assert parsing_stats.oldest == msg.timestamp_dt
        assert parsing_stats.newest == msg.timestamp_dt

        oldest = msg.timestamp_dt

        msg = Message.from_str("<41>Dec  3 22:14:15 daqe 1234")
        parsing_stats.add_msg(msg)

        assert parsing_stats.oldest == oldest
        assert parsing_stats.newest == msg.timestamp_dt

        parsing_stats.combine(parsing_stats)

        assert parsing_stats.total_count == 8
        assert parsing_stats['localhost'].total_count == 6
        assert parsing_stats['daqe'].total_count == 2

        assert "Alert total: 2" in parsing_stats.pprint_format()
