from bsdlog.__main__ import main
from bsdlog import generate


def test_generate(tmpdir, capsys):
    infile = tmpdir.mkdir('dir').join('input.log')
    generate.main(["--num", "100"])
    captured = capsys.readouterr()
    infile.write(captured.out)
    main(["--proc", "3", str(infile)])
    captured = capsys.readouterr()

    assert "Total messages: 100" in captured.out
