# flake8: noqa
import sys
import os
import pytest
from bsdlog.__main__ import main


def test_main(tmpdir, capsys, monkeypatch):

    def osopen(*args):
        raise PermissionError(args)

    with pytest.raises(SystemExit):
        main(["/non/existent/path/file.log"])

    with monkeypatch.context() as mpc:
        mpc.setattr(os, 'open', osopen)
        with pytest.raises(SystemExit):
            main(["/non/existent/path/file.log"])

    infile = tmpdir.mkdir('dir').join('input.log')
    with open(infile, 'wb') as inf:
        inf.write(
            b"<47>Sep 22 15:38:21 mymachine myproc% fatal error, terminating!\n"
            b"<34>Jan 25 05:06:34 10.1.2.3 su: 'su root' failed for sprinkles on /dev/pts/8\n"
            b"<47>Sep 22 15:38:21 mymachine myproc% fatal error, terminating!\n"
            b"<34>Jan 25 05:06:34 10.1.2.3 su: 'su root' failed for sprinkles on /dev/pts/8"
        )
    main([str(infile), "--proc", "2", "--chunk", "1"])
    captured = capsys.readouterr()

    assert "mymachine" in captured.out
    assert "Total messages: 4" in captured.out

    main([str(infile), "--proc", "1"])
    captured = capsys.readouterr()

    assert "mymachine" in captured.out
    assert "Total messages: 4" in captured.out

    with monkeypatch.context() as mpc:
        mpc.setattr(sys, 'stdin', open(infile))
        main(["--proc", "3"])
    captured = capsys.readouterr()
    assert "mymachine" in captured.out
    assert "Total messages: 4" in captured.out
